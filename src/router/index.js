import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/create",
    name: "Create",
    component: () =>
      import("../views/Create.vue")
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import("../views/About.vue")
  },
  {
    path: "/planet/:id",
    name: "Planet",
    component: () =>
        import("../views/Planet.vue")
  },
  {
    path: "/claim",
    name: "Claim",
    component: () =>
        import("../views/Claim.vue")
  },
  {
    path: "/*",
    name: "Home",
    component: Home
  }
];

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes
});

export default router;
